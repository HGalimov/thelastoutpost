﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Monster : MonoBehaviour
{
    public int health = 100;
    public float speed;
    public int damage = 10;
    public GameObject main_bldg;
    public float radius_attack;
    public GameObject[] towers;
    public float time_reloading;
    public bool is_shooted = false;
    public float tm;
    public GameObject bullet;
    // Use this for initialization
    void Start()
    {
        main_bldg = GameObject.FindWithTag("Base");
    }

    // Update is called once per frame
    void Update()
    {
        towers = GameObject.FindGameObjectsWithTag("Tower");
        if (health <= 0)
        {
            GameObject.Find("Bank").GetComponent<Bank>().money += 20;
            GameObject.Find("Level").GetComponent<Level>().monsters.Remove(gameObject);
            Destroy(gameObject);
        }
        foreach (GameObject obj in towers)
        {
            var dist = Vector3.Distance(transform.position, obj.transform.position);
            if (dist <= radius_attack && is_shooted == false && obj)
            {
                if (obj.GetComponent<Tower>().built == true)
                {
                    var tmp = Instantiate(bullet, transform.position, bullet.transform.rotation) as GameObject;
                    tmp.GetComponent<Bullet>().target_pos = obj.transform.position;
                    is_shooted = true;
                }
            }
            if (time_reloading <= Time.time)
            {
                is_shooted = false;
                time_reloading = Time.time + tm;
            }
        }
        if (main_bldg)
        {
            var dist_to_base = Vector3.Distance(transform.position, main_bldg.transform.position);
            if (dist_to_base <= 2)
                main_bldg.GetComponent<Base>().health--;
        }
        else
            Destroy(gameObject);
    }
}
