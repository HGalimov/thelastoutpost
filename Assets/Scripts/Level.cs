﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    public List<Transform> points = new List<Transform>();
    public GameObject monster;
    private Vector3 monster_spawn_pos;
    private float interval_x;
    private float interval_z = 2;
    public List<GameObject> monsters;
    public bool buy_clicked = false;
    public GameObject tower;
    public GameObject[] tower_locations;
    public bool tower_selected = true;
    public GameObject btn_buy_tower;
    public GameObject btn_cancel_buy_tower;
    public GameObject btn_ready;
    public bool cancel_buy = false;
    public bool start_enemy_attack = false;
    public int count_of_waves;
    private int count_line = 1;
    public float tm;
    public int line;
    public Text wave;
    private int cur_num_wave;
    public Canvas canvas;
    public GameObject tower_location;
    public Transform tower_loc_up;
    public Transform tower_loc_down;
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            Instantiate(tower_location, tower_loc_up.position, Quaternion.identity);
            tower_loc_up.position = new Vector3(tower_loc_up.position.x + 2, tower_loc_up.position.y, tower_loc_up.position.z);

        }
        for (int i = 0; i < 7; i++)
        {
            Instantiate(tower_location, tower_loc_down.position, Quaternion.identity);
            tower_loc_down.position = new Vector3(tower_loc_down.position.x + 2, tower_loc_down.position.y, tower_loc_down.position.z);

        }
        tower_locations = GameObject.FindGameObjectsWithTag("TowerLocation");
        monster_spawn_pos = points[0].position;
        tm = Time.time + 1;
    }

    public void BuyTower()
    {
        if (buy_clicked == false)
        {
            buy_clicked = true;

        }

    }
    public void CancelBuyTower()
    {
        if (cancel_buy == false)
            cancel_buy = true;

    }
    public void StartEnemyAttack()
    {
        if (start_enemy_attack == false)
        {
            start_enemy_attack = true;
            cur_num_wave++;

        }

    }
    // Update is called once per frame
    void Update()
    {
        wave.text = "wave" + "\n" + cur_num_wave + "/" + count_of_waves;
        if (count_line <= count_of_waves)
        {
            if (start_enemy_attack == true)
            {
                if (tm <= Time.time)
                {
                    monster_spawn_pos = points[0].position;

                    interval_x = 0;
                    interval_z = 2;

                    for (int i = 0; i < 3; i++)
                    {
                        var mnst = Instantiate(monster, monster_spawn_pos, Quaternion.identity) as GameObject;
                        mnst.GetComponent<Moving>().interval_x = interval_x;
                        mnst.GetComponent<Moving>().interval_z = interval_z;
                        monsters.Add(mnst);
                        monster_spawn_pos = new Vector3(monster_spawn_pos.x + 1, monster_spawn_pos.y, monster_spawn_pos.z);
                        interval_x++;
                        interval_z--;
                    }
                    line++;
                    tm = Time.time + 1;
                }
                if (line == count_line)
                {
                    start_enemy_attack = false;
                    line = 0;
                    count_line++;
                }
            }


            if (start_enemy_attack == false && monsters.Count == 0)
            {
                btn_ready.SetActive(true);
            }
            else
            {
                btn_ready.SetActive(false);
            }
        }
        if (monsters.Count == 0 && cur_num_wave == count_of_waves)
        {
            canvas.GetComponent<ControlUI>().end_num = 1;

        }

        if (buy_clicked == false)
        {
            btn_buy_tower.SetActive(true);
            btn_cancel_buy_tower.SetActive(false);
        }
        else
        {
            btn_buy_tower.SetActive(false);
            btn_cancel_buy_tower.SetActive(true);
        }

        Vector3 mouse_pos = Input.mousePosition;
        Vector3 obj_pos = Camera.main.ScreenToWorldPoint(mouse_pos);
        obj_pos = new Vector3(obj_pos.x, 1, obj_pos.y / 2);
        if (buy_clicked == true)
        {
            foreach (GameObject twr_loc in tower_locations)
            {
                if (twr_loc.GetComponent<TowerLocation>().under_tower != true)
                    twr_loc.SetActive(true);
            }
            if (tower_selected == true)
            {
                var obj = Instantiate(tower, obj_pos, Quaternion.identity) as GameObject;
                tower_selected = false;
                obj.GetComponent<Tower>().is_building = true;
            }
        }
        else
            foreach (GameObject twr_loc in tower_locations)
                twr_loc.SetActive(false);
    }
}
