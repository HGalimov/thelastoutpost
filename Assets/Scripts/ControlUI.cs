﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ControlUI : MonoBehaviour
{

    public GameObject level;
    public GameObject level_ui;
    public GameObject btn_start_game;
    public bool start_game = false;
    public GameObject win;
    public GameObject loss;
    public int end_num = 2;
    public float tm;
    public bool is_reload_scene = false;
    // Use this for initialization
    void Start()
    {

    }

    public void StartGame()
    {
        if (start_game == false)
        {
            start_game = true;
        }
    }
    // Update is called once per frame
    void Update()
    {      
        if (start_game == true)
        {
            level.SetActive(true);
            level_ui.SetActive(true);
            btn_start_game.SetActive(false);
        }
        else
        {
            level.SetActive(false);
            level_ui.SetActive(false);
            btn_start_game.SetActive(true);
        }
        switch (end_num)
        {
            case 0: loss.SetActive(true);

                level.SetActive(false);
                level_ui.SetActive(false);
                if (is_reload_scene == false)
                {
                    tm = Time.time + 2f;
                    is_reload_scene = true;
                }               
                break;


            case 1: win.SetActive(true);
                level.SetActive(false);
                level_ui.SetActive(false);
                if (is_reload_scene == false)
                {
                    tm = Time.time + 2f;
                    is_reload_scene = true;
                    
                }                 
                break;
        }
        if (Time.time >= tm && is_reload_scene == true) 
            SceneManager.LoadScene(0);
    }
}
