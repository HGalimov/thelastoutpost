﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shooting : MonoBehaviour
{

	public Collider[] colliders;
	public GameObject bullet;
	public float radius_attack;
	public List<GameObject> monsters;
	public float time_reloading;
	public bool is_shooted = false;
	public float tm;
	// Use this for initialization
	void Start ()
	{
		time_reloading = Time.time + tm;
	}
	
	// Update is called once per frame
	void Update ()
	{		
		monsters = GameObject.Find("Level").GetComponent<Level>().monsters;
		foreach (GameObject obj in monsters) {
			var dist = Vector3.Distance (transform.position, obj.transform.position);
			if (dist <= radius_attack && is_shooted == false && obj) {
				var tmp = Instantiate (bullet, transform.position, bullet.transform.rotation) as GameObject;
				tmp.GetComponent<Bullet> ().target_pos = obj.transform.position;
				is_shooted = true;
			}
			if (time_reloading <= Time.time) {
				is_shooted = false;
				time_reloading = Time.time + tm;
			}
		}
	}
}
