﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Moving : MonoBehaviour
{	
	public List<Transform> points;
	private bool in_turn = false;
	private GameObject level;  
	public float interval_x;
	public float interval_z;
	public Vector3 turn_pos;
	public Vector3 end_pos;
    

	// Use this for initialization
	void Start ()
	{	
		
		level = GameObject.Find ("Level");
		points = level.GetComponent<Level> ().points;
		turn_pos = new Vector3 (points [1].position.x + interval_x, points [1].position.y, points [1].position.z + interval_z);
		end_pos = new Vector3 (points [2].position.x, points [2].position.y, points [2].position.z + interval_z);
	}
	
	// Update is called once per frame
	void Update ()
	{		
		float step = gameObject.GetComponent<Monster>().speed * Time.deltaTime;
		if (transform.position == turn_pos) in_turn = true;
		if (in_turn == false) {
			transform.position = Vector3.MoveTowards (transform.position, turn_pos, step);
		}
		if (in_turn == true)
			transform.position = Vector3.MoveTowards (transform.position, end_pos, step);
	}
}
