﻿using UnityEngine;
using System.Collections;

public class TowerHealth : MonoBehaviour {

    public int health;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        health = gameObject.GetComponentInParent<Tower>().health;
        gameObject.GetComponent<TextMesh>().text = health.ToString();
	}
}
