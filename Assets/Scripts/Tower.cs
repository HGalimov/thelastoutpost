﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour
{

    public bool built = false;
    public int health = 100;
    public Vector3 mouse_pos;
    public bool is_building = false;    
    public int price;
    public GameObject tower_loc;
    public GameObject main_bldg;
    public Level level;
    public Bank bank;
    public Shooting shooting;
    // Use this for initialization
    void Start()
    {
        level = GameObject.Find("Level").GetComponent<Level>();
        bank = GameObject.Find("Bank").GetComponent<Bank>();
        shooting = gameObject.GetComponentInChildren<Shooting>();
        main_bldg = GameObject.FindGameObjectWithTag("Base");

    }


    // Update is called once per frame
    void Update()
    {        
        if (is_building == true)
        {
            Vector3 mouse_pos = Input.mousePosition;
            Vector3 obj_pos = Camera.main.ScreenToWorldPoint(new Vector3(mouse_pos.x, mouse_pos.y, 10f));
            obj_pos = new Vector3(obj_pos.x, 1, obj_pos.z * 1.5f);
            if (built == false)
            {
                shooting.enabled = false;
                transform.position = Vector3.MoveTowards(transform.position, obj_pos, 1f);
                if (Input.GetMouseButtonDown(0))
                {
                    RaycastHit hit;
                    if (Physics.Raycast(transform.position, -Vector3.up, out hit) && hit.collider.tag == "TowerLocation" && bank.money >= price)
                    {
                        tower_loc = hit.collider.gameObject;
                        transform.position = new Vector3(hit.collider.transform.position.x, 1, hit.collider.transform.position.z);
                        tower_loc.GetComponent<TowerLocation>().under_tower = true;
                        built = true;
                        is_building = false;
                        bank.money -= price;
                        level.buy_clicked = false;
                        level.tower_selected = true;
                        shooting.enabled = true;
                    }
                }
                if (Input.GetMouseButtonDown(1) || level.cancel_buy == true)
                {
                    level.buy_clicked = false;
                    level.tower_selected = true;
                    level.cancel_buy = false;
                    Destroy(gameObject);
                }
            }
        }
        if (health <= 0)
        {
            tower_loc.GetComponent<TowerLocation>().under_tower = false;
            Destroy(gameObject);
        }
        if (main_bldg) { 
            if (!main_bldg.activeInHierarchy)
                Destroy(gameObject);
            }            
        else      
            Destroy(gameObject);
    }
}
