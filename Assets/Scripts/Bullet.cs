﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    public float speed;
    public int damage;
    public Vector3 target_pos;
    // Use this for initialization
    void Start()
    {

    }
    void OnCollisionEnter(Collision coll)
    {
        if (gameObject.tag == "Bullet")
        {
            if (coll.collider.tag == "Monster")
            {
                coll.gameObject.GetComponent<Monster>().health -= damage;
                Destroy(gameObject);
            }
        }
        if (gameObject.tag == "EnemyBullet")
        {
            if (coll.collider.tag == "Tower")
            {
                coll.gameObject.GetComponent<Tower>().health -= damage;
                Destroy(gameObject);

            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target_pos, step);
        if (transform.position == target_pos)
            Destroy(gameObject);

    }
}
