﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Base : MonoBehaviour {

    public int health = 1000;
    public Text health_bar;
    public Canvas canvas;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (health > 0)
            health_bar.text = health.ToString();            
        else
        {
            health_bar.text = "0";
            Destroy(gameObject);
            canvas.GetComponent<ControlUI>().end_num = 0; 
        }
            
	}
}
